module.exports = {
  type: 'link/group-profile',
  tangle: 'link',
  staticProps: {
    parent: { $ref: '#/definitions/cloakedMessageId', required: true },
    child: { $ref: '#/definitions/messageId', required: true },
    parentGroupId: { $ref: '#/definitions/cloakedMessageId' } // for subgroups, this represents the parent group
  },
  props: {}
}
