const { isMsgId } = require('ssb-ref')
const test = require('tape')

const Server = require('../../test-bot')

const emptyProfileFields = {
  legalName: null,
  altNames: [],
  description: null,
  city: null,
  country: null,
  postCode: null,
  profession: null,
  education: [],
  school: [],
  gender: null,
  source: null,
  aliveInterval: null,
  deceased: null,
  placeOfBirth: null,
  placeOfDeath: null,
  buriedLocation: null,
  birthOrder: null,
  avatarImage: null,
  headerImage: null,
  tombstone: null,
  customFields: {}
}

test('ssb.profile.person.group.create (recps)', t => {
  const server = Server()

  const input = {
    authors: {
      add: [server.id]
    }
  }

  server.profile.person.group.create(input, (err) => {
    t.match(err.message, /A private profile\/person must be encrypted only to a group/, 'throws an error when missing recps on create')

    server.close(t.end)
  })
})

test('ssb.profile.person.group.create (unallowed input)', t => {
  const server = Server({ tribes: true })
  server.tribes.create({}, (_, { groupId, groupInitMsg } = {}) => {
    const input = {
      authors: {
        add: [server.id]
      },
      phone: 'not sure',
      recps: [groupId]
    }

    server.profile.person.group.create(input, (err) => {
      t.match(err.message, /unallowed inputs: phone/, 'doesnt allow additional props')

      server.close(t.end)
    })
  })
})

test('ssb.profile.person.group.*', t => {
  const server = Server({ tribes: true })
  server.tribes.create({}, (_, { groupId, groupInitMsg } = {}) => {
    const input = {
      preferredName: 'Alice',
      legalName: 'Alison Wonderland',
      source: 'ahau',
      authors: {
        add: [server.id]
      },
      recps: [groupId]
    }

    server.profile.person.group.create(input, (err, profileId) => {
      t.error(err, 'creates a private person profile')

      t.true(isMsgId(profileId), 'returns valid profileId')
      server.profile.person.group.get(profileId, (err, profile) => {
        t.error(err, 'gets private person profile')

        const state = {
          ...emptyProfileFields,
          tombstone: null,
          source: 'ahau',
          preferredName: 'Alice',
          legalName: 'Alison Wonderland',
          authors: {
            [server.id]: [{ start: 1, end: null }]
          }
        }
        t.deepEqual(
          profile,
          {
            key: profileId,
            type: 'profile/person',
            originalAuthor: server.id,
            recps: [groupId],
            states: [{
              type: 'person',
              key: profileId,
              ...state
            }],
            ...state,
            conflictFields: []
          },
          'profile state is correct'
        )

        server.profile.person.group.update(profileId, { gender: 'female' }, (err, updateId) => {
          t.error(err, 'updates the profile')

          server.profile.person.group.get(profileId, (err, updatedProfile) => {
            t.error(err, 'gets the updated profile')

            const state = {
              ...emptyProfileFields,
              preferredName: 'Alice',
              legalName: 'Alison Wonderland',
              gender: 'female',
              source: 'ahau',
              authors: {
                [server.id]: [{ start: 1, end: null }]
              }
            }
            t.deepEqual(
              updatedProfile,
              {
                key: profileId,
                type: 'profile/person',
                originalAuthor: server.id,
                recps: [groupId],
                states: [
                  {
                    type: 'person',
                    key: updateId,
                    ...state
                  }
                ],
                ...state,
                conflictFields: []
              },
              'returns the updated profile'
            )

            server.profile.person.group.tombstone(profileId, { reason: 'user deleted account' }, (err, tombstoneId) => {
              t.error(err, 'tombstones profile')

              server.profile.person.group.get(profileId, (err, tombstonedProfile) => {
                t.error(err, 'gets tombstoned profile')

                const state = {
                  ...emptyProfileFields,
                  preferredName: 'Alice',
                  legalName: 'Alison Wonderland',
                  gender: 'female',
                  source: 'ahau',
                  tombstone: {
                    reason: 'user deleted account',
                    date: tombstonedProfile.states[0].tombstone.date
                  },
                  authors: {
                    [server.id]: [{ start: 1, end: null }]
                  }
                }
                t.deepEqual(
                  tombstonedProfile,
                  {
                    key: profileId,
                    type: 'profile/person',
                    originalAuthor: server.id,
                    recps: [groupId],
                    states: [
                      {
                        type: 'person',
                        key: tombstoneId,
                        ...state
                      }
                    ],
                    ...state,
                    conflictFields: []
                  },
                  'returns the tombstoned profile'
                )

                server.close(t.end)
              })
            })
          })
        })
      })
    })
  })
})

test('ssb.profile.person.group.get (will not get public profile)', t => {
  const server = Server()

  const content = {
    type: 'profile/person',
    preferredName: { set: 'Alice Public' },
    tangles: {
      profile: { root: null, previous: null }
    }
    // recps
  }
  server.publish(content, (err, m) => {
    t.error(err, 'creates a public profile')

    const profileId = m.key

    server.profile.person.group.get(profileId, (err) => {
      t.match(err.message, /A private profile\/person must be encrypted only to a group/, 'cant get public profile using private person profile api')

      server.close(t.end)
    })
  })
})

// check compatibility with older person profiles (with recps set)
test('ssb.profile.person.group.get (works with legacy profiles)', t => {
  const server = Server({ tribes: true })
  server.tribes.create({}, (_, { groupId, groupInitMsg } = {}) => {
    const content = {
      type: 'profile/person',
      preferredName: { set: 'Alice Private' },
      location: 'Wonderland', // NOTE deprecated, but want to test it doesnt mess up private person get
      // authors // NOTE ideally we would set this but ssb-crut currently set it when it's missing
      tangles: {
        profile: { root: null, previous: null }
      },
      recps: [groupId]
    }

    server.publish(content, (err, msg) => {
      t.error(err, 'creates a legacy private person profile')
      const profileId = msg.key

      server.profile.person.group.get(profileId, (err, profile) => {
        t.error(err, 'can get private person profile saved using old api')

        const state = {
          preferredName: 'Alice Private',
          authors: profile.states[0].authors, // lazy!
          ...emptyProfileFields
        }
        t.deepEqual(
          profile,
          {
            key: profileId,
            type: 'profile/person',
            originalAuthor: server.id,
            recps: [groupId],
            states: [
              {
                key: profileId,
                type: 'person',
                ...state
              }
            ],
            ...state,
            conflictFields: []
          },
          'returns the profile'
        )

        server.close(t.end)
      })
    })
  })
})
